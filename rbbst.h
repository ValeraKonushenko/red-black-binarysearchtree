#pragma once
#include "Pair.h"
//#include "Stack.h"
#define DEBUG
#ifdef DEBUG
#include "Console.h"
#endif
#ifndef VK_NAMESPACE
#define VK_NAMESPACE
#define VK_NAMESPACE_BEG namespace vk{
#define VK_NAMESPACE_END }
#endif

VK_NAMESPACE_BEG
enum NodeColor :bool {
	RED,
	BLACK
};
/********************************************************/
/*--------------------BASE PREDICATES-------------------*/
/********************************************************/
template<class Key>
struct BasePrEq {
	virtual bool operator()(Key &a, Key &b) = 0;
};
template<class Key>
struct BasePrBigger {
	virtual bool operator()(Key &a, Key &b) = 0;
};
template<class Key>
struct BasePrBiggerEq {
	virtual bool operator()(Key &a, Key &b) = 0;
};
template<class Key>
struct BasePrLess {
	virtual bool operator()(Key &a, Key &b) = 0;
};

template<class Key>
struct DefPrEq : public BasePrEq<Key> {
	bool operator () (Key &a, Key &b)override{
		if (a == b)return true;
		return false;
	}
};
template<class Key>
struct DefPrBigger : public BasePrBigger<Key> {
	bool operator () (Key &a, Key &b)override {
		if (a > b)return true;
		return false;
	}
};
template<class Key>
struct DefPrBiggerEq : public BasePrBiggerEq<Key> {
	bool operator () (Key &a, Key &b)override {
		if (a >= b)return true;
		return false;
	}
};
template<class Key>
struct DefPrLess : public BasePrLess<Key> {
	bool operator () (Key &a, Key &b)override {
		if (a < b)return true;
		return false;
	}
};





/********************************************************/
/*-------------------------NODE-------------------------*/
/********************************************************/
template<class Key, class Value>
class Node{
public:
	Key		key;
	Value	value;
	NodeColor color;
	Node	*right, *left, *parent;
	Node(Node *r, Node *l, Node *p, Key &key, Value &value, NodeColor color = RED) :
		key(key), value(value), color(color), right(r), left(l), parent(p) {}
};


/********************************************************/
/*-----------------------ITERATOR-----------------------*/
/********************************************************/
template<class Key, class Value>
class BaseRBBSTIterator {
protected:
	Node<Key,Value> *node;
public:
	bool IsEmpty() {
		return !node;
	}
};



/********************************************************/
/*--------------------RED-BLACK-BST---------------------*/
/********************************************************/
template<class Key, class Value,
	class PrEq = DefPrEq<Key>,
	class PrBigger = DefPrBigger<Key>,
	class PrBiggerEq = DefPrBiggerEq<Key>,
	class PrLess = DefPrLess<Key>>
class RBBST {
public:
	using type = Node<Key, Value>;
	class Iterator : public BaseRBBSTIterator<Key, Value>{
	private:
		friend class RBBST<Key, Value>;
		Iterator(type *node) {
			this->node = node;
		}
	public:
		Iterator() {
			this->node = nullptr;
		}
		Iterator(const Iterator&it) {
			this->node = it.node;
		}
		Pair<const Key&, Value&> operator()() {
			return { this->node->key, this->node->value };
		}
	};
private:
	PrBiggerEq	pr_bgger_eq;
	PrBigger	pr_bigger;
	PrLess		pr_less;
	PrEq		pr_eq;
	unsigned int size;
	type *root;
	void RotateLeft(type *x){
		type *fake = x->right;
		if (root == x)root = fake;
		fake->parent = x->parent;
		if (x->parent)
			if (x->parent->right == x)
				x->parent->right = fake;
			else
				x->parent->left = fake;
		x->right = fake->left;
		if(fake->left)		fake->left->parent = x;
		fake->left = x;
		x->parent = fake;
	}
	void RotateRight(type *x) {
		type *fake = x->left;
		fake->parent = x->parent;
		if (root == x)root = fake;
		if(x->parent)
			if(x->parent->right == x)
				x->parent->right = fake;
			else
				x->parent->left = fake;
		x->left = fake->right;
		if(fake->right)		fake->right->parent = x;
		fake->right = x;
		x->parent = fake;
	}

	void InsertFixup(type *x) {
		//pointer to NULL node is NIL = BLACK node
		while (x != root && x->parent && x->parent->parent && x->parent->color == RED) {
			if (x->parent == x->parent->parent->right) {
				type *fake = x->parent->parent->left;
				if (!fake || fake->color == BLACK) {
					if (x == x->parent->left) {
						x = x->parent;
						RotateRight(x);
					}
					x->parent->color = BLACK;
					x->parent->parent->color = RED;
					RotateLeft(x->parent->parent);
				}
				else if (fake->color == RED) {
					x->parent->color = BLACK;
					fake->color = BLACK;
					x->parent->parent->color = RED;
					x = x->parent->parent;
				}
			}
			else {
				type *fake = x->parent->parent->right;
				if (!fake || fake->color == BLACK) {
					if (x == x->parent->right) {
						x = x->parent;
						RotateLeft(x);
					}
					x->parent->color = BLACK;
					x->parent->parent->color = RED;
					RotateRight(x->parent->parent);
				}
				else if (fake->color == RED) {
					x->parent->color = BLACK;
					fake->color = BLACK;
					x->parent->parent->color = RED;
					x = x->parent->parent;
				}
			}
		}
		root->color = BLACK;
	}
	void DeleteFixup(type *x) {

	}
public:
	RBBST() {
		size = 0;
		root = nullptr;
	}
	Iterator Begin() {
		return Iterator(root);
	}
	bool Insert(Key key, Value &&value) {
		type *fake = root;
		type *parent = nullptr;
		if (!fake) {
			root = new type(nullptr, nullptr, nullptr, key, value, BLACK);
			size++;
			return true;
		}
		while (fake) {
			parent = fake;
			//if (pr_eq(key, fake->key))	return false;
			if (pr_bgger_eq(key, fake->key))	fake = fake->right;
			else					fake = fake->left;
		}
		if (pr_bgger_eq(key, parent->key)) {//>
			parent->right = new type(nullptr, nullptr, parent, key, value);
			InsertFixup(parent->right);
		}
		else{
			parent->left = new type(nullptr, nullptr, parent, key, value);
			InsertFixup(parent->left);
		}						

		size++;
		return true;
	}
	bool Insert(Key key, Value &value) {
		type *fake = root;
		type *parent = nullptr;
		if (!fake) {
			root = new type(nullptr, nullptr, nullptr, key, value, BLACK);
			size++;
			return true;
		}
		while (fake) {
			parent = fake;
			//if (pr_eq(key, fake->key))	return false;
			if (pr_bgger_eq(key, fake->key))	fake = fake->right;
			else					fake = fake->left;
		}
		if (pr_bgger_eq(key, parent->key)) {//>
			parent->right = new type(nullptr, nullptr, parent, key, value);
			InsertFixup(parent->right);
		}
		else {
			parent->left = new type(nullptr, nullptr, parent, key, value);
			InsertFixup(parent->left);
		}

		size++;
		return true;
	}
	Iterator Find(Key &&key) {
		type *fake = root;
		while (fake) {
			if (fake == nullptr || pr_eq(fake->key, key))break;
			if (pr_bgger_eq(key, fake->key))fake = fake->right;
			else if (pr_less(key, fake->key))fake = fake->left;
		}
		if(fake == nullptr) return Iterator();
		return Iterator(fake);
	}
	Iterator Find(Key &key) {
		type *fake = root;
		while (fake) {
			if (fake == nullptr || pr_eq(fake->key, key))break;
			if (pr_bgger_eq(key, fake->key))fake = fake->right;
			else if (pr_less(key, fake->key))fake = fake->left;
		}
		if (fake == nullptr) return Iterator();
		return Iterator(fake);
	}
	bool Delete(Key key) {
		type *fake = Find(key).node;
		if (!fake)return false;
		//I'm considering 3 cause in deliting of smth:
		//1. When a node has only ONE childs
		//2. When a node has TWO childs
		//3. When a node has NO childs





		return true;
	}
#ifdef DEBUG
#define SHIFT_X 4
#define SHIFT_Y 3
private:
	using Color = _Console::ConsoleColor;
	void PrvtShow(type *node, SHORT x, SHORT y) {
		if (!node)return;
		type *fake = node;
		if(fake->parent && fake == fake->parent->left)
			while (fake) {
				fake = fake->right;
				if(fake)x -= 3;
			}
		else if(fake->parent)
			while (fake) {
				fake = fake->left;
				if (fake)x += 3;
			}

		if (node->color == RED)
			Console.SetColor(Color::Red, Color::BrightWhite);
		else
			Console.SetColor(Color::Black, Color::BrightWhite);
		
		Console.SetCursorPositon(x - 1, y);
		Console.Write(node->key);
		Console.SetColor(Color::BrightWhite, Color::Black);

		PrvtShow(node->left,  x - SHIFT_X, y + SHIFT_Y);
		PrvtShow(node->right, x + SHIFT_X, y + SHIFT_Y);
	}
public:
	void Show() {
		system("cls");
		PrvtShow(root, Console.GetCols() / 2, 0);
	}
#endif // DEBUG
};
VK_NAMESPACE_END